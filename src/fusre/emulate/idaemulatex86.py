import os
import imp
import sys
import time
import getopt
import threading
import traceback


from vivisect import VivWorkspace
from envi.archs.i386 import i386Module, i386Disasm
from vivisect.impemu import i386WorkspaceEmulator, Amd64WorkspaceEmulator


try:
    import idc, idautils, idaapi
except:
    print "This Emulator should probably be loaded in IDA Pro :(, sorry"
    raise




class IDAEmulatorState(object):
    def __init__(self, emu):
        self.emu = emu
        self.previous_address = self.emu.getRegisterByName('eip')
        self.current_address = self.emu.getRegisterByName('eip')

    def setPreviousAddress(self):
        try:
            idc.SetColor(self.previous_address, idc.CIC_ITEM, 0xffffff)
        except:
            pass

        self.previous_address = self.current_address
        self.current_address = self.emu.getRegisterByName('eip')
        try:
            idc.SetColor(self.previous_address, idc.CIC_ITEM, 0xa0a0ff)
        except:
            pass
    
    def setCurrentAddress(self):
        self.setPreviousAddress()
        self.current_address = self.emu.getRegisterByName('eip')
        result = False
        try:
            result = idc.Jump(self.current_address)
            if result:
                idc.SetColor(self.current_address, idc.CIC_ITEM, 0xffa0a0)
                return result
        except:
            # might ask to load a file here?
            pass
        return False


# Not sure if building the additional Emulator state into the class
# is such a good idea, but we'll see what happens.  At the very
# least, it makes it easy to affect the emulator without writing
# a bunch of code or copying and pasting.  
class IDAEmuWrapperi386(i386WorkspaceEmulator):
    def __init__(self, vw, logwrite=False, logread=False):
        i386WorkspaceEmulator.__init__(self, vw, logwrite, logread)
        self.state = IDAEmulatorState(self)
    
    def stepi(self):
        i386WorkspaceEmulator.stepi(self)
        self.state.setCurrentAddress()

class IDAEmuWrapperAMD64(Amd64WorkspaceEmulator):
    def __init__(self, vw, logwrite=False, logread=False):
        Amd64WorkspaceEmulator.__init__(self, vw, logwrite, logread)
        self.state = IDAEmulatorState(self)

    def stepi(self):
        Amd64WorkspaceEmulator.stepi(self)
        self.state.setCurrentAddress()




class IDAVivChild(VivWorkspace):
    workspace_emus  = {
        "i386"  :IDAEmuWrapperi386,
        "amd64" :IDAEmuWrapperAMD64,
    }

    def __init__(self, fname = None, arch=None):
        VivWorkspace.__init__(self)
        print "Here we go again"
        self.setMeta('Architecture', arch)
        if arch is None:
            self.setMeta('Architecture', 'i386')
        
        self.fname = ''
        if fname:
            self.fname = fname

        self._init_mem_maps()

    def getIDABytes(self, start, end):
        get_byte = lambda addr: chr(idc.Byte(addr)) if idc.isLoaded(addr) else '\x00'
        return "".join([get_byte(b) for b in xrange(start, end) ])

    def _init_mem_maps(self):
        self.segments = dict([(idc.SegName(s), (s, idc.SegEnd(s))) for s in idautils.Segments()])
        for name in self.segments:
            start, end = self.segments[name]
            bytes = self.getIDABytes(start, end)
            perms = idc.GetSegmentAttr(start, idc.SEGATTR_PERM)
            self.addMemoryMap(start, perms, '%s.%s'%(self.fname, name), bytes)

    def updateSegmentByName(self, segname):
        start, end = self.segments[segname]
        bytes = self.getIDABytes(start, end)
        self.writeMemory(start, bytes)

    def updateSegmentByName(self, segname):
        start, end = self.segments[segname]
        bytes = self.getIDABytes(start, end)
        self.writeMemory(start, bytes)



    # ganked this out of the parent class, since I want to override what 
    # happens....minutae
    def getEmulator(self, logwrite=False, logread=False):
        """
        Get an instance of a WorkspaceEmulator for this workspace.

        Use logread/logwrite to enable memory access tracking.
        """
        arch = self.getMeta("Architecture").lower()
        # here is the minor change
        eclass = self.workspace_emus.get(arch)
        if eclass == None:
            raise Exception("WorkspaceEmulation not supported on %s yet!" % arch)
        return eclass(self, logwrite=logwrite, logread=logread)


    