from service.networking import Message
from service.node.basenode import BaseNode
from idahandlers import *
from basehandler import *
from idapython_aliases import *
from idabridgeutils import *
from time import *

IDAPRO_CMD = 0x01

PERFORM_IDA_CMD = 0x00
RESULTS_IDA_CMD = 0x01
SYNC_CLOCK = 0x02
REQ_TYPE = 0
RESP_TYPE = 1

MSG_TYPES = { PERFORM_IDA_CMD:'perform_ida_cmd',
              RESULTS_IDA_CMD:'ida_cmd_results',
              SYNC_CLOCK:'sync_clock',
              'ida_cmd_results':PERFORM_IDA_CMD,
              'perform_ida_cmd':RESULTS_IDA_CMD,
              'sync_clock':SYNC_CLOCK}



class IDAProNode(BaseNode):
    def __init__(self, server = None, port = 19955,  request_spacing = .5, 
        consumer_timer = .5, allowed_pending = 10):
    	
    	BaseNode.__init__(self, server, port, request_spacing, 
    		consumer_timer, allowed_pending)



    def handle_msg(self, src, msg):
        '''
        Handler that gets called when a message needs to be processed

        '''
        data = msg.data()
        kargs = json.loads(data)


        if 'fn_call' in kargs and kargs.get('fn_call') is True:
        	if not 'fn_name' in kargs and
        	   not 'fn_args' in kargs:
        	   failure = "'fn_args' or 'fn_call' not present in the 'fn_call' request"
        	   self.handle_job_failure('fn_call', src, msg, failure)
        	
        	args = kargs.get('fn_args')
        	fn = kargs.get('fn_call')


        elif 'cmd' in kargs:
        	if not 'fn_name' in kargs and
        	   not 'fn_args' in kargs:
        	   failure = "'fn_args' or 'fn_call' not present in the 'fn_call' request"
        	   self.handle_job_failure('fn_call', src, msg, failure)





        
        
    def handle_job_failure(self, job, src, msg, failure_str = "Job failed to get sent"):
        '''
        Handler that gets called when  a message failed

        '''
        pass
        
        
    def handle_job_completion(self, job, src, msg):
        '''
        Handler that gets called when a job is completed
        '''
        pass
