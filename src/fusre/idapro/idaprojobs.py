from fusre.service.job import BaseJob

try:
    import idc
    import idaapi
    import idautils
    import pywraps
except:
    print "unable to load the ida modules"
    raise


class IDAProCall(BaseJob):
    '''
    classdocs
    '''


    def __init__(self, parent, **kargs):

        BJ_kargs = {}
        for i in BaseJob.init_kargs:
            if i in kargs:
                BJ_kargs[i] = kargs[i]
                
        BaseJob.__init__(self, **BJ_kargs)

		self.fn = fn
		self.fn_args = fn_args



	def call_fn(self):
		f = self.look_up()
		print f
		print self.fn_args
		print f(*self.fn_args)
		return f(*self.fn_args)


	def look_up(self):
		a = self.fn
		if a in idc.__dict__:
			return getattr(idc, a)

		if a in idautils.__dict__:
			return getattr(idautils, a)

		if a in idaapi.__dict__:
			return getattr(idaapi, a)

		return None

    def target(self, *args):
        
        # perform requested action
        self.call_fn()
        self.result_set = True
        # done performing requested action
        self.finished()