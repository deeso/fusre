from service.networking.message import Message
from service.networking.asynchsocket import MessageSocket

IDAPRO_CMD = 0x01
PERFORM_IDA_CMD = 0x00
RESULTS_IDA_CMD = 0x01
SYNC_CLOCK = 0x02

REQ_TYPE = 0
RESP_TYPE = 1

MSG_TYPES = { PERFORM_IDA_CMD:'perform_ida_cmd',
              RESULTS_IDA_CMD:'ida_cmd_results',
              SYNC_CLOCK:'sync_clock',
              'ida_cmd_results':PERFORM_IDA_CMD,
              'perform_ida_cmd':RESULTS_IDA_CMD,
              'sync_clock':SYNC_CLOCK}

class BasicIdaClient(object):
	def __init__(self, server_host, 
		port, sslkeyfile, sslcertfile):
		self.host = server_host
		self.port = port
		self.clock = 0
		self.msg_sock = MessageSocket(host, port, self.certfile, self.keyfile)
        msg_sock.register_handler('recv', self.recv_msg)
        msg_sock.register_handler('send', self.sent_msg)

    def update_clock(self, clock_val):
        self._clock_lock.acquire()
        if self._logical_clock < clock_val:
            self._logical_clock = clock_val
        self._clock_lock.release()
        return self._logical_clock
    
    def increment_clock(self):
        self._clock_lock.acquire()
        self._logical_clock += 1
        v = self._logical_clock
        self._clock_lock.release()
        return v

    def get_clock(self):
        return self._logical_clock

	def send_fn_call(self, fn_name, *args):
		jdata = {"fn_call":True, 
		          "fn_name":fn_name,
		          "fn_args":args}

        data = json.dumps(jdata)
        
        msg_type = MSG_TYPES['perform_ida_cmd']
        

        msg_ts = self.get_clock()
        msg_id = msg_ts
            
        out_msg = Message(msg_type, msg_id, msg_ts, data)
        self.send(src, out_msg)

    def sent_msg(self, msg):
    	print ("Handling the post send of some data")
    	self.increment_clock()


    def recv_msg(self, instance):
        '''
        handler called when the underlying network inteface has a completed message that needs to be 
        processed.  The handler will parse the message determine the type and pass the message off to the
        handler that handles the particular message type.
        
        instance: lower layer network abstraction that performed the operation
        '''

        while instance.has_recv_msg():
        	self.increment_clock()
            msg = instance.next_recv_msg()
            self.update_clock(msg.msg_ts())
            
            msg_type = msg.msg_type()
            src = instance.getpeername()
            if not msg_type in self._recv_handlers:
                print("Dont know how to handle this type of message: %d from %s"%(msg_type, str(instance.getpeername())))
            #handler = self._recv_handlers[msg_type]
            print ("Recv'ed the folling information: msg_type(%s) id(%d) ts(%d) data(%s)"% \
            	    (MSG_TYPES[msg_type], msg.msg_id(), msg.msg_ts(), base64.decodestring(msg.data())))
