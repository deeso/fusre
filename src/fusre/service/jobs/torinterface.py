'''
 Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

@author: Adam Pridgen <adam.pridgen@thecoverofnight.com>
'''



from subprocess import *
from socket import *
from time import sleep

import random
import os
import shlex
from crawler.jobs.basejob import BaseJob
from crawler.util.Socks5Interface import Socks5Socket

class RunTor(BaseJob):
    '''
    Class starts a tor process and its based on:
    http://blog.databigbang.com/distributed-scraping-with-multiple-tor-circuits/
    '''
    DAEMON_OPT = "--RunAsDaemon %i"
    COOKIE_AUTH_OPT = "--CookieAuthentication %i"
    HASHED_PASS_OPT = "--HashedControlPassword \"%s\""
    CONTROL_PORT_OPT = "--ControlPort %i"
    PID_OPT = "--PidFile %s"
    SOCK_OPT = "--SocksPort %i"
    DATA_DIR_OPT = "--DataDirectory %s"
    CMD = "tor"
    def __init__(self, port, control_port = None, cookie_auth = 0, 
                 data_dir = "/tmp/data/", hashed_pass = "", host = "127.0.0.1", 
                 timeout = 2):
        '''
        initialize and start the tor interface.  the interface is a basically 
        home grown socks5 proxy wrapped around tor.
        
        The parameter types are explicit, because these parameters will be used
        to form a command string that will then run the tor with those command line 
        arguments.  e.g.:
        
        tor --CookieAuthentication 0 --PidFile tor247443675.pid --SocksPort 9050 \
            --DataDirectory /tmp/data/tor247443675 --HashedControlPassword ""
        
        parameters:
        port(required, int), this will be the port that tor binds to on the localhost,
             and that the proxy communicates with
        control_port (None, int): control port that can be used to manage tor while it is running
        cookie_auth (0, int) CookieAuthentication parameter for tor
        data_dir (/tmp/data, str):  DataDirectory, this is then appended with tor<random int> which
                           is used to make up the pid file
        hashed_pass ("", str): HashedPass used for connecting to the control port
        host ('127.0.0.1', str): host interface on which to bind tor
        timeout(10, int): timeout of the tor clients interface
        
        '''
        
        BaseJob.__init__(self)
        self._port = port
        self._control_port = control_port


        pid_file = random.randint(0, 0xFFFFFFF)
        self._pidfile = "tor"+str(pid_file)+".pid"
        while os.path.exists("/var/run/"+str(pid_file)+".pid"):
            pid_file = random.randint(0, 0xFFFFFFF)
            self._pidfile = "tor"+str(pid_file)+".pid"
            
        self._cookie_auth = cookie_auth
        self._data_dir = data_dir+"tor"+str(pid_file)
        if not os.path.exists(self._data_dir):
            if not os.path.exists(os.path.split(self._data_dir)[0]):
                os.makedirs(os.path.split(self._data_dir)[0])
            os.mkdir(self._data_dir)
        self._hashed_pass = hashed_pass
        
        self._host = host
        self.update_cmd_str()
        self._pid = None
        self._process = None
        self._alive = False
        self._run = False
        self._proxy_interface = None
        os.environ['PATH'] = ':'.join([os.getenv('PATH'), "/usr/sbin/", "/opt/local/bin"])
        self._timeout = timeout

    
    def update_cmd_str(self):
        '''
        update the internal command string
        '''
        self._cmd = " ".join(self.get_command())
        
    def get_args(self):
        '''
        Fill out the argument values for the various flags in tor
        '''
        result = []
        #result.append(self.DAEMON_OPT%self._run_as_daemon)
        result.append(self.COOKIE_AUTH_OPT%self._cookie_auth)
        if not self._control_port is None:
            result.append(self.CONTROL_PORT_OPT%self._control_port)
        result.append(self.PID_OPT%self._pidfile)
        result.append(self.SOCK_OPT%self._port)
        if len(self._data_dir) > 2:
            result.append(self.DATA_DIR_OPT%self._data_dir)
        result.append(self.HASHED_PASS_OPT%self._hashed_pass)
        return result
    
    def get_cmd(self):
        '''
        get the internal command string for tor
        '''
        return self.CMD
    
    def get_command(self):
        '''
        Create a list of command line arguments to execute tor
        '''
        r = self.get_args()
        r.insert(0, self.get_cmd())
        return r
    
    def target(self):
        '''
        used to start the tor stuff using a thread
        '''
        self.start_tor()

    def finished(self):
        pass
    
    def stop_tor(self):
        if self._alive:
            self._process.terminate()
    
    def start_tor(self):
        '''
        Start an instance of tor using the command line and the provided arguments.
        '''
        self._run = True
        # code to start the tor command here
        self.update_cmd_str()
        args = ['/bin/sh', '-c'] + self.get_command()
        args = "/bin/sh -c '"+self._cmd+"'"
        self._process = Popen(args, shell=True, stdout=PIPE, stderr=PIPE  )
        self._pid = self._process.pid
        self._alive = True
        self.create_tor_socks_intf()
        # done code to start the tor command
        return self._pid
    
    def create_tor_socks_intf(self):
        '''
        Initialize a Socks5Interface for this instance of tor
        '''
        self._proxy_interface = Socks5Socket(self._host, self._port, self._timeout)

        
    
    def send_recv_http_request(self, host, port, data):
        '''
        Send the data through the Socks5Interface
        host: host to connect too
        port: port of the endpoint
        data: data to send
        '''
        return self._proxy_interface.send_recv_http_request(host, port, data)
    
    def reverse_lookup(self, ipaddr):
        '''
        Perform a host reverse look-up
        ipaddr: ascii format of the IP address to look up.  Currently only IPv4 is supported
        '''
        return self._proxy_interface.get_ip_resolution_req(ipaddr)
    
    def name_lookup(self, name):
        '''
        Perform a host name look-up
        ipaddr: ascii name of the host to look up.  Currently only IPv4 is supported
        '''
        return self._proxy_interface.get_name_resolution_req(name)
    
    def connect(self, host, port):
        '''
        Connect to the host on port through the Socks5Interface over tor
        '''
        return self._proxy_interface.connect(host, port)

    def open_proxy_socket(self):
        '''
        Initialize a Socks5 connection to tor through the socks5interface 
        '''
        return self._proxy_interface.open_socks5_connection()
    
    def read_stdout(self):
        '''
        read the tor process's stdout
        '''
        return self._process.communicate()[0]
    
    def isAlive(self):
        '''
        poll the process for a process returncode, and this indicates whether 
        or not the process is still alive.  If the returncode is not None,
        then the process is considered dead 
        
        '''
        self._process.poll()
        if not self._process.returncode is None:
            self._alive = False
        return self._alive
    
