'''
 Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

@author: Adam Pridgen <adam.pridgen@thecoverofnight.com>
'''


import types


class BaseJob(object):
    '''
    BaseJob is a Job class that needs to be extended for the various tasks 
    to be completed
    '''
    
    init_kargs = ['priority', 'cb_method', 'cb_args']
    def __init__(self, args = (), priority = 0, cb_method = None, cb_args = ()):
        '''
        BJ.__init__ -- requires t
        '''
        self._priority = 0
        self._args = args
        if self._args is None:
            self._args = ()
        self.set_cb_meth(cb_method)
        self.set_cb_args(cb_args)
    
    def target(self, *args):
        '''
        BJ.target -- needs to be extended
        '''
        raise Exception("Failed to define a Real Target")
    
    def set_cb_meth(self, cb = None):
        '''
        BJ.set_cb_meth -- Set the call back method once this job completes
        '''
        if not isinstance(cb, types.FunctionType) and not isinstance(cb, types.MethodType) and\
            not isinstance(cb, types.LambdaType):
            cb = None
        self._cb_meth = cb
    
    def set_cb_args(self, args = ()):
        '''
        BJ.set_cb_args -- Set the call back arguments once this job completes
        '''
        self._cb_args = args
    
    def set_cb_meth_args(self, cb = None, args = ()):
        '''
        BJ.set_cb_meth_args -- set the call back method and the arguments for job
        '''
        self.set_cb_args(args)
        self.set_cb_meth(cb)
        
    def finished(self):
        '''
        BJ.finished -- Execute the call back and the method if these have been set 
        '''
        if not self._cb_meth is None and\
           isinstance(self._cb_args, tuple):
            self._cb_meth(self, *self._cb_args)
        
        elif not self._cb_meth is None:
            self._cb_meth(self)
        
    def priority(self):
        '''
        BJ.priority -> integer -- returns the specified priority for this job
        '''
        return self._priority
    
    def args(self):
        '''
        BJ.args() -> tuple -- returns the tuple of arguments for the job with self in position 0
        '''
        return (self,) + self._args
    
    
    def add_self_to_cb_args(self):
        '''
        BJ.add_self_to_cbargs() -- add self in position 0 of the call back arguments
        '''
        return (self,) + self._cb_args

    
    def set_priority(self, priority):
        '''
        BJ.set_priority -- set the specified priority for this job
        '''
        self._priority = priority