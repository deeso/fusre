'''
 Copyright 2011 Adam Pridgen <adam.pridgen@thecoverofnight.com>

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

@author: Adam Pridgen <adam.pridgen@thecoverofnight.com>
'''
from crawler.jobs.basejob import BaseJob
import sys
import traceback




HTTP_OK = 'HTTP/1.1 200'
class TorHttpRequest(BaseJob):
    '''
    classdocs
    '''


    def __init__(self, tor_interface, **kargs):
        '''
        Constructor
        
        tor_interface is a RunTor class that will be used to send the request.
        
        Key word arguments include:
        url: url to the host
        host: host of the web server
        port: int port that the remote webserver is running on
        headers: {} of keys and values, where the keys are the header names, and the values
        are the values of the headers
        proxy: (string, int) proxy to connect through after tor, but this is not used.
        
        '''
        BJ_kargs = {}
        for i in BaseJob.init_kargs:
            if i in kargs:
                BJ_kargs[i] = kargs[i]
                
        BaseJob.__init__(self, **BJ_kargs)

        self._tor_interface = tor_interface
        self._url = kargs.get("url", None)
        self._host = kargs.get("host", None)
        self._port = kargs.get("port", 80)
        self._headers = kargs.get("headers", {})
        self._proxy = kargs.get("proxy", None)
        self.req_str = ''
        if not isinstance(self._proxy, tuple):
            self._proxy = None
        elif len(self._proxy) != 2 or not (isinstance(self._proxy[0], str) or\
                  isinstance(self._proxy[1], int)):
            self._proxy = None
                    
        if self._url is None:
            raise Exception("Url is required for operation!")
        
        if self._host is None:
            raise Exception("Host is required for operation!")
        
        self.result = None
        self.result_set = False
        
    def target(self, *args):
        
        # perform requested action
        uri = self.get_uri_string()
        host_str = self.get_host_string()
        headers = self.get_header_string()
        self.req_str = uri + host_str + headers + "\r\n"
        self.result = self.execute_request()
        self.result_set = True
        # done performing requested action
        self.finished()
                
    def get_uri_string(self):
        '''
        THR.get_uri_string -> str 
        
        Returns the first line of a HTTP GET request, e.g. "GET / HTTP/1.1"
        '''
        
        return "GET " + self._url + " HTTP/1.1\r\n"
    
    def get_host_string(self):
        '''
        THR.get_host_string -> str 
        
        Returns the 'Host' header if the header key is not present in the Headers for the request
        '''
        
        if "host" in [i.lower() for i in self._headers.keys()]:
            return ''
        return "Host: "+self._host+"\r\n"
    
    def get_header_string(self):
        '''
        THR.get_header_string -> str 
        
        Returns all the headers converted into a string
        '''        
        headers = []
        for i in self._headers.keys():
            headers.append(i+': '+self._headers[i] )
        return "\r\n".join(headers) + "\r\n"
        
    def execute_request(self):
        '''
        THR.execute_request -> str
        
        Send the request through the open socket to the remote host.  
        
        3 attempts will be made, even if there is an exception.  Upon an 
        excepation the problem is noted in the result 
        
        '''
        
        data = (False, "")
        retrys = 3
        while retrys >= 0:
            try:
                result, data = self._tor_interface.send_recv_http_request(self._host, self._port, self.req_str)
                
                if result < 0 or len(data) > 0:
                    return data
            except Exception as e:
                # improper acquisition
                tb = traceback.format_exc(sys.exc_info())
                print (tb)
                data = tb
                break
            retrys -= 1
        return data
